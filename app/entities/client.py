from sqlalchemy import Column, Integer, String, DateTime, func
from app.db.session import Base

class Client(Base):
    __tablename__ = "clients"

    id = Column(Integer, primary_key=True, index=True)
    first_name = Column(String, index=True)
    last_name = Column(String, index=True)

    def save(self, db):
        db.add(self)
        db.commit()

    @staticmethod
    def getAll(db):
        apps = db.query(Client).all()
        return apps
    
    @staticmethod
    def deleteOne(id, db):
        db.query(Client).filter(Client.id == id).delete()
        db.commit()
        
    @staticmethod
    def deleteAll(db):
        db.query(Client).delete()
        db.commit()

    def __str__(self):
       return f"app(id={self.id}, name={self.name}, email={self.kubeconfig_file_id})"