from pydantic import BaseModel


class ClientSchema(BaseModel):
    id: int
    first_name: str
    last_name: str

class ClientAddSchema(BaseModel):
    first_name: str
    last_name: str