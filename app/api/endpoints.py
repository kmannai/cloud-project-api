import json
from fastapi import APIRouter, Depends, File, UploadFile
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session
from app.db.session import get_db
from app.entities.client import Client
from app.schemas.client import ClientAddSchema
from app.utils.custom_encoder import AlchemyEncoder
from app.core.config import Settings

router = APIRouter()

settings = Settings()

@router.get("/client")
def get_clients(db: Session = Depends(get_db)):
    clients = Client.getAll(db)
    result = json.loads(json.dumps(clients, cls = AlchemyEncoder, skipkeys=True))
    return JSONResponse(content = result, status_code = 200)

@router.post("/client")
def add_client(client: ClientAddSchema, db: Session = Depends(get_db)):
    new_client: Client = Client(**client.dict())
    new_client.save(db)
    return JSONResponse(content = {"message": "client added","client_id": new_client.id}, status_code = 201)

@router.delete("/client/{id}")
def add_client(id: int, db: Session = Depends(get_db)):
    Client.deleteOne(id, db)
    return JSONResponse(content = {"message": "client deleted","client_id": id}, status_code = 201)
